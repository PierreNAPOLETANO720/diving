from django.contrib import admin
from app.models     import Photo, Video

admin.site.register(Photo)
admin.site.register(Video)