from django.db import models

class Photo(models.Model):
    class Meta:
        verbose_name        = 'Photo'
        verbose_name_plural = 'Photos'

    name        = models.CharField (verbose_name='Nom de la photo',          max_length=50)
    country     = models.CharField (verbose_name='Pays',                     max_length=50)
    image       = models.ImageField(verbose_name='Image',                    upload_to='images/')
    description = models.TextField (verbose_name='Description de la photos', blank=True, null=True)

    def __str__(self) -> str:
        return f'Nom: {self.name} - Pays: {self.country}'

class Video(models.Model):
    class Meta:
        verbose_name        = 'Video'
        verbose_name_plural = 'Videos'

    name        = models.CharField(verbose_name='Nom de la vid\xe9o',         max_length=50)
    country     = models.CharField(verbose_name='Pays',                       max_length=50)
    video       = models.FileField(verbose_name='Vid\xe9o',                   upload_to='videos/')
    description = models.TextField(verbose_name='Description de la vid\xe9o', blank=True, null=True)

    def __str__(self) -> str:
        return f'Nom: {self.name} - Pays: {self.country}'