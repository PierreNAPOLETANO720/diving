from django.shortcuts          import render
from django.views.generic.base import TemplateView
from app.models                import Photo, Video

class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Didier CAPDEROU - Page d\'accueil'
        return context

class PhotoView(TemplateView):
    template_name = 'photos.html'
    model         = Photo

    def get_context_data(self, *args, **kwargs):
        context = super(PhotoView, self).get_context_data(*args, **kwargs)
        context['title']  = 'Didier CAPDEROU - Galerie photos'
        context['photos'] = Photo.objects.all()
        return context

class VideoView(TemplateView):
    template_name = 'videos.html'
    model         = Video

    def get_context_data(self, *args, **kwargs):
        context = super(VideoView, self).get_context_data(*args, **kwargs)
        context['title']  = 'Didier CAPDEROU - Galerie vidéos'
        context['videos'] = Video.objects.all()
        return context

class ContactView(TemplateView):
    template_name = 'contact.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ContactView, self).get_context_data(*args, **kwargs)
        context['title'] = 'Didier CAPDEROU - Page de contact'
        return context